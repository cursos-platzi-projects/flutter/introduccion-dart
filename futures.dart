//Se le puso async a la funcion para poder usar el await
void main() async {
  print('Antes de la peticion');

  final data = await httpGet('http://api.nasa.com/aliens');

  print(data);
  // final nombre = await getNombre('18');
  // print(nombre);
  // getNombre('10').then( print );

  print('Fin de la peticion');
}

//Para poder hacer uso del async, se debe devolder un Future
Future<String> getNombre( String id ) async {
  return '$id - Gianni';
}

//Simular una peticion http GET
//Future = es una tarea asincrona que se hace a un hilo diferente a nuestra aplicacion.
Future<String> httpGet( String url ) {
  return Future.delayed( Duration( seconds: 3 ), () {
    return 'Hola mundo - 3 segundos';
  });
}