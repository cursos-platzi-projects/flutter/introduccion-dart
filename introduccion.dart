void main() {
  
  //Strings
  /*var nombre = "Tony";
  var apellido = "Stark";*/
  //final nombre = "Tony"; //Esto es como una constante
  //const nombre = "Tony"; //Tambien es una constante
  //final String nombre = "Tony"; //Esto es como una constante
  String nombre = "Tony";
  String apellido = "Stark";
  
  nombre = 'Peter';
  print('$nombre $apellido');
  
  //Numeros
  int empleados = 10;
  double salario = 1854.15;
  
  print(empleados);
  print(salario);

  /**** Boleanos */
  //bool isActive = null; no se puede asignar null
  bool? isActive = null; // Sepone ? para poderle asignar el null

  if(isActive == null) {
    print("Esto es null");
  }

  /**** Listas */
  List numeros = [1,2,3,4,5,6,7,8,9,10];
  //Esto es posible por que la lista es Dynamic
  numeros.add("Gianni");
  print(numeros);
  //List<int> -- Asi es como especifica que tipo de dato

  //Esta funcion de List genera 100 numeros aleatorios
  final masNumeros = List.generate(100, (int index) => index);
  print(masNumeros);

  /**** Mapas */
  //Este mapa es valido
  Map persona = {
    'nombre': 'Gianni',
    'edad': 15,
    'soltero': false,
    true: false,
    1: 100,
    2: 500
  };

  //Para agregar al mapa
  persona.addAll({3: 'Juan'});

  //Para acceder a sus valores
  print( persona['true'] );

  //----------------------
  Map<String, dynamic> persona2 = {
    'nombre': 'Gianni',
    'edad': 15,
    'soltero': false,
  };

  persona.addAll({'segundoNombre': 'Juan'});
  print( persona );
}