//El 'as' es opcional
import 'dart:math' as math;

void main() {
  final cuadrado = new Cuadrado( 5 );

  //Esto junto lo de abajo seria el set
  cuadrado.lado = 20;

  print( 'area: ${cuadrado.calculaArea()}' );

  print( 'lado: ${cuadrado.lado}' );

  print( 'area get: ${cuadrado.area}' );

}

class Cuadrado {
  double lado;

  //Esto es un getter
  double get area {
    return this.lado * this.lado;
  }

  set area( double valor) {
    // print('set: $valor');
    //sqrt = Funcion para hacer la raiz cuadrada
    this.lado = math.sqrt(valor);
  }

  Cuadrado( double lado ):
    this.lado = lado;

  double calculaArea() {
    return this.lado * this.lado;
  }
}