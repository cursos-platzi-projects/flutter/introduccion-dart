void main() {
  final nombre = 'Gianni';
  saludar(nombre, 'Greetings');
  saludar2(nombre: nombre);//output: null Gianni
  saludar3(nombre: nombre, mensaje: 'Hola'); //output: Gianni Hola

}

//[ String mensaje = 'Hi' ] para setear parametros opcionales
void saludar(String nombre, [ String mensaje = 'Hi' ]) {
  print('$mensaje $nombre');
}

//String? -- El mensaje puede ser null
void saludar2({ String nombre = 'No name', String? mensaje }) {
  print('$mensaje $nombre');
}

//required = poner que son obligatorio los parametros
void saludar3({ required String nombre, required String mensaje }) {
  print('$mensaje $nombre');
}