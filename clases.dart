void main() {

  final rawJson = {
    'nombre': 'Tony Stark',
    'poder': 'Dinero'
  };

  // final ironman = new Heroe(nombre: rawJson['nombre']!, poder: rawJson['poder']!);
  // print (ironman);

  final ironman = Heroe.fromJson(rawJson);
  print(ironman);

  //En dart el new es opcional pero siempre es bueno ponerlo
  final wolverin = new Heroe(nombre: 'Logan', poder: 'Regeneracion');

  // wolverin.nombre = 'Logan';
  // wolverin.poder = 'Regeneracion';

  print( wolverin );
}

class Heroe {
  String nombre; //Afuerzas deben llevar el signo ? si no se asigna valor
  String poder;

  //Constructor
  // Heroe(String pNombre) {
  //   this.nombre = pNombre;
  // }

  //required para que obligatoriamente envien los parametros
  Heroe({
    required this.nombre,
    required this.poder
  });

  //Los 2 puntos significa que esta funcion se va a ejecutar en el momento en que se esta creando la instancia de la clase
  Heroe.fromJson( Map<String, String> json ):
    this.nombre = json['nombre']!,
    this.poder = json['poder'] ?? 'No tiene poder';

  @override
  String toString() {
    return 'Heroe: nombre: ${this.nombre}, poder: ${this.poder}';
  }
}