void main() {
  final perro = new Perro();
  final gato = new Gato();

  perro.emitirSonido();
  gato.emitirSonido();

  sonidoAnimal(perro);
  sonidoAnimal(gato);
}

void sonidoAnimal( Animal animal) {
  animal.emitirSonido();
}

//Las clases abstractas solo sirven para implementar las funciones, parecido a las interfaces en Java
abstract class Animal {
  int? patas;

  void emitirSonido();

}

class Perro implements Animal {
  int? patas;

  void emitirSonido() => print("Guauuuuuuuuu");
}

class Gato implements Animal {

  int? patas;
  int? cola;

  void emitirSonido() => print('Miauuuuuuu');
}